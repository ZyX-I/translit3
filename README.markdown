
This plugin provides the ability to transform one sequence of symbols into 
another. The typical use case is transliteration of latin symbols into russian 
symbols for systems without russian keyboard.


Features:

  - transliteration of text, provided as function argument;

  - transliteration of a selection or a line range;

  - input transliteration;

  - setting up the input transliteration in current buffer or in all opened

    buffers;

  - plugin system for transliteration function;

  - plugin system for input transliteration;

  - loading transliteration table from a file, a variable, a dictionary or

    a function;

  - displaying the transliteration table on the screen.


Plugin requires some additional plugins:

  - [frawor](https://bitbucket.org/ZyX_I/frawor)

  - [jsonvim](http://bitbucket.org/ZyX_I/jsonvim)

(with their dependencies).

Documentation is available online at [http://vimpluginloader.sourceforge.net/doc/translit3.txt.html](http://vimpluginloader.sourceforge.net/doc/translit3.txt.html).
Russian documentation is at [http://vimpluginloader.sourceforge.net/doc/translit3.rux.html](http://vimpluginloader.sourceforge.net/doc/translit3.rux.html).
